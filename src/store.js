import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// Used for default chart options
const DefaultOptions = {
  name: null,
  limit: 50,
  year: 2019
};

export default new Vuex.Store({
  state: {
    error: null,
    isLoading: false,
    options: {
      ...DefaultOptions
    },
    selection: null
  },
  mutations: {
    setError(state, msg) {
      state.error = msg;
    },
    clearError(state) {
      state.error = null;
    },
    setLoading(state, status) {
      state.isLoading = status;
    },
    setOptions(state, options) {
      state.options = { ...options };
    },
    setSelection(state, selection) {
      state.selection = { ...selection };
    },
    clearSelection(state) {
      state.selection = null;
    }
  },
  actions: {
    setError(ctx, msg) {
      ctx.commit("setError", msg);
    },
    clearError(ctx) {
      ctx.commit("clearError");
    },
    startLoading(ctx) {
      ctx.commit("setLoading", true);
    },
    stopLoading(ctx) {
      ctx.commit("setLoading", false);
    },
    resetOptions(ctx) {
      ctx.commit("setOptions", DefaultOptions);
    },
    setOptions(ctx, options) {
      ctx.commit("setOptions", options);
    },
    setSelection(ctx, selection) {
      ctx.commit("setSelection", selection);
    },
    clearSelection(ctx) {
      ctx.commit("clearSelection");
    }
  }
});
